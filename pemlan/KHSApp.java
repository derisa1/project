package KHS;

import java.util.ArrayList;
import java.util.Scanner;

public class KHSApp {
    private ArrayList<Mahasiswa> mahasiswaList;
    private Scanner scanner;

    public KHSApp() {
        mahasiswaList = new ArrayList<>();
        scanner = new Scanner(System.in);
    }
        public void tambahData() {
            System.out.println("╔════════════════════════════════════════════════════════════╗");
            System.out.println("║                      TAMBAH DATA MAHASISWA                 ║");
            System.out.println("╚════════════════════════════════════════════════════════════╝");
            System.out.println("Masukkan NIM Mahasiswa:");
            String nim = scanner.next();
            System.out.println("Masukkan Nama Mahasiswa:");
            String nama = scanner.next();
    
            Mahasiswa mahasiswa = new Mahasiswa(nim, nama);
    
            System.out.println("Masukkan Kode MK:");
            String kodeMK = scanner.next();
            System.out.println("Masukkan Nama MK:");
            String namaMK = scanner.next();
            System.out.println("Masukkan Nilai Angka:");
            int nilaiAngka = scanner.nextInt();
    
            mahasiswa.tambahMataKuliah(kodeMK, namaMK, nilaiAngka);
            mahasiswaList.add(mahasiswa);
            System.out.println(" ");
            System.out.println("Data berhasil ditambahkan!");
        }
        public void cetakKHS() {
            System.out.println("╔════════════════════════════════════════════════════════════╗");
            System.out.println("║                  KARTU HASIL STUDI (KHS)                   ║");
            System.out.println("╚════════════════════════════════════════════════════════════╝");
            for (Mahasiswa mhs : mahasiswaList) {
                System.out.println("Mahasiswa: " + mhs.nim + " - " + mhs.nama);
                System.out.println("+-----------+--------------------------------+--------+");
                System.out.println("| Kode MK   | Nama MK                        | Nilai  |");
                System.out.println("+-----------+--------------------------------+--------+");
                
                for (MataKuliah mk : mhs.mataKuliahList) {
                    System.out.printf("| %-9s | %-30s | %-6s |\n", mk.kode, mk.nama, nilaiHuruf(mk.nilaiAngka));
                }
                
                System.out.println("+-----------+--------------------------------+--------+");
            }
        }            
        
        private char nilaiHuruf(int nilaiAngka) {
            if (nilaiAngka >= 80 && nilaiAngka <= 100) {
                return 'A';
            } else if (nilaiAngka >= 60 && nilaiAngka < 80) {
                return 'B';
            } else if (nilaiAngka >= 50 && nilaiAngka < 60) {
                return 'C';
            } else if (nilaiAngka >= 40 && nilaiAngka < 50) {
                return 'D';
            } else {
                return 'E';
            }
        }
    
        public static void main(String[] args) {
            KHSApp app = new KHSApp();
            Scanner scanner = new Scanner(System.in);
            int choice;
            do {
                System.out.println(" ");
                System.out.println("╔════════════════════════════════════════════════════════════╗");
                System.out.println("║                           MENU                             ║");
                System.out.println("╠════════════════════════════════════════════════════════════╣");
                System.out.println("║ 1. Tambah Data                                             ║");
                System.out.println("║ 2. Cetak KHS                                               ║");
                System.out.println("║ 3. Keluar                                                  ║");
                System.out.println("╚════════════════════════════════════════════════════════════╝");
                System.out.println(" ");
                System.out.println("Pilih menu (1/2/3):");
                System.out.println(" ");
                choice = scanner.nextInt();
                System.out.println(" ");
                switch (choice) {
                    case 1:
                        app.tambahData();
                        break;
                    case 2:
                        app.cetakKHS();
                        break;
                    case 3:
                        System.out.println("Terima kasih!");
                        break;
                    default:
                        System.out.println("Pilihan tidak valid, silakan pilih kembali.");
                }
            } while (choice != 3);
            scanner.close();
        }
    }