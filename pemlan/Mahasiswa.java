package KHS;
import java.util.ArrayList;

public class Mahasiswa {
    String nim;
    String nama;
    ArrayList<MataKuliah> mataKuliahList;

    public Mahasiswa(String nim, String nama) {
        this.nim = nim;
        this.nama = nama;
        this.mataKuliahList = new ArrayList<>();
    }

    public void tambahMataKuliah(String kode, String nama, int nilaiAngka) {
        MataKuliah mk = new MataKuliah(kode, nama, nilaiAngka);
        mataKuliahList.add(mk);
    }
}